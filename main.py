import sys
import subprocess
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import itertools


# not being used anymore
cache_types = [['lru', 1, '2', '2', 'wb'], ['lfu', '1', '2', '4', 'wb'], ['lru', '2', '1', '4', 'wb'], ['lfu', '2', '1', '4', 'wb'],
               ['lru', '4', '1', '1', 'wb'], ['lfu', '4', '4', '1', 'wb'], [
                   'lru', '1', '1', '4', 'wb'], ['lru', '2', '1', '4', 'wb'],
               ['lru', '8', '4', '4', 'wb'], ['lru', '8', '1', '4', 'wb'], [
                   'lru', '8', '1', '4', 'wb'], ['lru', '8', '1', '2', 'wb'],
               ['lru', '2', '8', '4', 'wb'], ['lru', '1', '1', '1', 'wb'], [
                   'lru', '2', '2', '2', 'wb'], ['lru', '1', '8', '1', 'wb'],
               ['lru', '8', '8', '4', 'wb'], ['lru', '8', '8', '8', 'wb'], [
                   'lru', '16', '1', '4', 'wb'], ['lru', '16', '4', '4', 'wb'],
               ['lru', '4', '16', '4', 'wb'], ['lru', '4', '1', '16', 'wb'], ['lru', '8', '4', '4', 'wb'], ['lru', '2', '2', '8', 'wb']]


def main():
    if(len(sys.argv) < 2):
        print("Please provide file as an argument!")
        exit(1)
    #use config from data.csv
    if(len(sys.argv) > 2 and sys.argv[2] == '1'):
        df = pd.read_csv("data.csv")
    #use Cartesian product from cache_config and generate possible tuples
    else:
        cache_config = pd.read_csv(
        'cache_config.csv', names=['Policy', 'Sets', 'Ways', 'Block_size', 'Write_policy'], keep_default_na=False)
        #extract each column in it's own list
        policies = [string for string in cache_config.Policy.tolist() if string != ""]
        sets = [string for string in cache_config.Sets.tolist() if string != ""]
        ways =  [string for string in cache_config.Ways.tolist() if string != ""]
        block_sizes = [string for string in cache_config.Block_size.tolist() if string != ""]
        write_policies = [string for string in cache_config.Write_policy.tolist() if string != ""]
        #make array of columns
        group = [policies, sets, ways, block_sizes, write_policies]
        #create a Cartesian product
        cartesian_product = list(itertools.product(*group))
        #save all in dataframe
        df = pd.DataFrame(cartesian_product, columns = ['Policy', 'Sets', 'Block_size', 'Ways', 'Write_policy'])

    #read program path
    file_path = sys.argv[1]

    # result of evaluating each qtmips_cli call with provided cache config
    cache_results = dict()

    
    for i, row in df.iterrows():
        #cache configuration command, ex : lru, 1,1,1,wb
        arg = ','.join(map(str, row))
        #final command
        bash_command = "qtmips_cli --asm {} --read-time 10 --write-time 10 --burst-time 1 --dump-cycles --d-cache {} --dump-cache-stats".format(
            file_path, arg)
        process = subprocess.Popen(
            bash_command.split(), stdout=subprocess.PIPE)
        #execute command
        output, error = process.communicate()
        #decode bytes received from qtmips_cli into list of string
        result = output.decode("utf-8").split('\n')
        #we interested only in rows starting with "d-cache"
        data_cache_stats = [x for x in result if 'd-cache:' in x]
        #parse results
        for row in data_cache_stats:
            #remove d-cache prefix
            row = row.replace('d-cache:', '')
            #split TEST CRITERIA:VALUE
            data = row.split(':')
            #collect all data in dictionary
            if data[0] in cache_results:
                # append the new number to the existing array at this slot
                cache_results[data[0]].append(float(data[1]))
            else:
                # create a new array in this slot
                cache_results[data[0]] = [float(data[1])]
    #insert all collected data in appropriate row in dataframe
    for key in cache_results.keys():
        df[key] = cache_results[key]
    #df.plot(y=["Sets", "Ways", "Block_size"], x="improved-speed")
    #plt.show()
    #select and print best cache configuration
    print("Best impoved speed config:")
    print(df.iloc[df['improved-speed'].argmax()])

   # print(df)


if __name__ == "__main__":
    main()
