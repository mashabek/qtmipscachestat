### About
This tool was created to automatize testing different cache configurations with https://github.com/cvut/QtMips tool. 
I don't have a great experience in Python, so pardon me for poor coding techniques.
This project is under development and maybe buggy.I was planning it to be a starting point for someone, who will be interested 
in contributing. 
Please follow instructions properly and do not try to test input validation, I've not added them yet.
### Quickstart
1. Be sure that you have installed - Python, Matplotlib, Pandas
2. If you want to test with predefined parameters, please add your testing cases in data.csv according the template and run:
    `python main.py path_to_file.S 1`
3. If you want to generate all possible tuples(Cartesian product), you can add more parameters in config_cache.csv and run:
    `python main.py path_to_file.S`
4. After evaluating, you should be able to see best cache configuration according to improvement speed in stdout(Plotting is disabled, will be added in future commits)
### Screenshots
![](https://i.imgur.com/24sdLa9.png)


